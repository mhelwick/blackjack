#include "card.h"

Card::Card() {
	this->drawn = false;
}

Card::Card(Suit suit, Face face) {
	this->suit = suit;
	this->face = face;
	this->drawn = false;
}
