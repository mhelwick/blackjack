#ifndef CARD_H
#define CARD_H

#include <iostream>
#include <string>

enum Suit {SPADE, CLUB, HEART, DIAMOND};
enum Face {ACE = 1, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING};

class Card {
private:
	Suit suit;
	Face face;
	bool drawn;
public:
	Card();
	Card(Suit suit, Face face);

	Suit getSuit() { return this->suit; }
	Face getFace() { return this->face; }

	void draw() { this->drawn = true; }
	bool isDrawn() { return this->drawn; }

	friend std::ostream &operator<<(std::ostream &output, Card &card) {
		std::string suitString, faceString;
		switch(card.getSuit()) {
			case SPADE: suitString = "Spades";
				break;
			case CLUB: suitString = "Clubs";
				break;
			case HEART: suitString = "Hearts";
				break;
			case DIAMOND: suitString = "Diamonds";
				break;
			default: break;
		}

		switch(card.getFace()) {
			case ACE: faceString = "Ace";
				break;
			case TWO: faceString = "Two";
				break;
			case THREE: faceString = "Three";
				break;
			case FOUR: faceString = "Four";
				break;
			case FIVE: faceString = "Five";
				break;
			case SIX: faceString = "Six";
				break;
			case SEVEN: faceString = "Seven";
				break;
			case EIGHT: faceString = "Eight";
				break;
			case NINE: faceString = "Nine";
				break;
			case TEN: faceString = "Ten";
				break;
			case JACK: faceString = "Jack";
				break;
			case QUEEN: faceString = "Queen";
				break;
			case KING: faceString = "King";
				break;
			default: break;
		}
		output << faceString << " of " << suitString << "\n";
		return output;
	}
};

#endif
