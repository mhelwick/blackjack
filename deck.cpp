#include <stdlib.h>
#include <time.h>
#include "deck.h"

Deck::Deck() {
	srand(time(NULL));
	this->inDeck = 52;

	int count = 0;

	for(int i = 1; i < 14; i++) {
		for(int j = 0; j < 4; j++) {
			this->cards[count] = new Card(Suit(j), Face(i));
			count++;
		}
	}
}

Card * Deck::draw() {
	int drawCard = rand() % 52;

	while(this->cards[drawCard]->isDrawn()) {
		drawCard = rand() % 52;
	}

	this->inDeck--;
	this->cards[drawCard]->draw();
	return this->cards[drawCard];
}
