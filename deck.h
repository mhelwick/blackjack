#ifndef DECK_H
#define DECK_H

#include "card.h"

class Deck {
private:
	Card * cards[52];
	int inDeck;
public:
	Deck();

	Card * draw();
	void reset();
};

#endif
