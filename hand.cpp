#include "hand.h"

void Hand::checkBust() {
	auto it = this->cards.begin();
	int total = 0;
	while(it != this->cards.end()) {
		if((*it)->getFace() == JACK || (*it)->getFace() == QUEEN || (*it)->getFace() == KING) {
			total += 10;
		} else {
			total += (*it)->getFace();
		}
		it++;
	}

	if(total > 21) {
		this->bust = true;
	}
}

void Hand::print() {
	auto it = this->cards.begin();

	while(it != this->cards.end()) {
		std::cout << **it;
		it++;
	}
	std::cout << std::endl;
}

int Hand::tally() {
	auto it = this->cards.begin();
	int total = 0;
	while(it != this->cards.end()) {
		if((*it)->getFace() == JACK || (*it)->getFace() == QUEEN || (*it)->getFace() == KING) {
			total += 10;
		} else {
			total += (*it)->getFace();
		}
		it++;
	}

	return total;
}

bool Hand::blackjack() {
	if(this->cards.front()->getFace() == ACE && (this->cards.back()->getFace() == KING || this->cards.back()->getFace() == QUEEN || this->cards.back()->getFace() == JACK)) {
		return true;
	} else if(this->cards.back()->getFace() == ACE && (this->cards.front()->getFace() == KING || this->cards.front()->getFace() == QUEEN || this->cards.front()->getFace() == JACK)) {
		return true;
	} else {
		return false;
	}
}

Card * Hand::split() {
	Card * splitCard = this->cards.back();
	this->cards.pop_back();
	return splitCard;
}
