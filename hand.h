#ifndef HAND_H
#define HAND_H

#include <vector>
#include "card.h"

class Hand {
private:
	std::vector<Card *> cards;
	bool bust;
	bool stand;
public:
	Hand() { this->bust = false; this->stand = false; }

	void addCard(Card * card) { this->cards.push_back(card); }
	void checkBust();
	bool isBust() { return this->bust; }
	void print();
	void setStand() { this->stand = true; }
	bool getStand() { return this->stand; }
	int tally();
	bool blackjack();
	Card * split();
};

#endif
