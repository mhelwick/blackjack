#include <iostream>
#include <string>
#include "deck.h"
#include "card.h"
#include "hand.h"

void deal(Hand &player, Hand &dealer, Deck deck);

void hit(Hand &player, Deck deck);

void win(Hand player, Hand dealer, Hand split, bool splitBool);

int main() {
	Deck deck;

	Hand player, dealer, split;
	bool splitBool = false;

	deal(player, dealer, deck);

	if(player.blackjack() && !dealer.blackjack()) {
		std::cout << "Player blackjack!\n";
		win(player, dealer, split, splitBool);
	} else if(!player.blackjack() && dealer.blackjack()) {
		std::cout << "Dealer blackjack!\n";
		win(player, dealer, split, splitBool);
	} else if(player.blackjack() && dealer.blackjack()) {
		std::cout << "Player and dealer blackjack!\n";
		win(player, dealer, split, splitBool);
	}

	std::string move;

	while(!player.getStand()) {
		if(!player.isBust()) {
			player.print();
			std::cout << "Hit or stand: ";
			std::cin >> move;

			if(move == "hit" || move == "Hit") {
				hit(player, deck);
				player.print();
				if(player.isBust()) {
					std::cout << "Player is bust!\n\n";
					player.setStand();
				}
			} else if(move == "double" || move == "Double") {
				hit(player, deck);
				player.print();
				if(player.isBust()) {
					std::cout << "Player is bust!\n\n";
				}
				player.setStand();
			} else if(move == "split" || move == "Split") {
				split.addCard(player.split());
				hit(player, deck);
				hit(split, deck);
				splitBool = true;
			} else {
				player.print();
				player.setStand();
			}
		}
	}

	if(splitBool) {
		while(!split.getStand()) {
			if(!split.isBust()) {
				split.print();
				std::cout << "Hit or stand: ";
				std::cin >> move;

				if(move == "hit" || move == "Hit") {
					hit(split, deck);
					split.print();
					if(split.isBust()) {
						std::cout << "Player is bust!\n\n";
						split.setStand();
					}
				} else {
					split.print();
					split.setStand();
				}
			}
		}
	}

	while(!dealer.getStand()) {
		if(dealer.tally() <= 16) {
			std::cout << "Dealer hits\n";
			hit(dealer, deck);
			dealer.print();
			if(dealer.isBust()) {
				std::cout << "Dealer is bust!\n";
				dealer.setStand();
			}
		} else {
			std::cout << "Dealer stand\n";
			dealer.setStand();
		}
	}

	win(player, dealer, split, splitBool);

	return 0;
}

void deal(Hand &player, Hand &dealer, Deck deck) {
	player.addCard(deck.draw());
	player.addCard(deck.draw());
	dealer.addCard(deck.draw());
	dealer.addCard(deck.draw());
}

void hit(Hand &player, Deck deck) {
	player.addCard(deck.draw());
	player.checkBust();
}

void win(Hand player, Hand dealer, Hand split, bool splitBool) {
	if(player.tally() > dealer.tally() && !player.isBust()) {
		std::cout << "Player wins!\n";
	} else if(player.tally() < dealer.tally() && !dealer.isBust()){
		std::cout << "Dealer wins!\n";
	} else if(player.isBust() && dealer.isBust()) {
		std::cout << "Both bust, no winner\n";
	} else if(player.tally() == dealer.tally() && !player.isBust() && !dealer.isBust()) {
		std::cout << "It's a tie!\n";
	}

	if(splitBool) {
		if(split.tally() > dealer.tally() && !split.isBust()) {
			std::cout << "Player split wins!\n";
		} else if(split.tally() < dealer.tally() && !dealer.isBust()){
			std::cout << "Dealer wins!\n";
		} else if(split.isBust() && dealer.isBust()) {
			std::cout << "Both bust, no winner\n";
		} else if(split.tally() == dealer.tally() && !split.isBust() && !dealer.isBust()) {
			std::cout << "It's a tie!\n";
		}
	}
}
